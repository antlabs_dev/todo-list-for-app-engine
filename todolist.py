import webapp2
import jinja2
import os
import hashlib
import json
import logging
from time import strftime, localtime
from google.appengine.ext import ndb
from google.appengine.api import users

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(str(os.path.dirname(__file__) + "/templates")), \
                                       extensions=["jinja2.ext.autoescape"])
DEFAULT_TODOLIST_NAME = "default_todolist"
DEFAULT_DONELIST_NAME = "default_donelist"
USE_AUTH_USER = True    # Enables user's authentfication. Don't forget  login: required and secure: always in app.yaml

logging.getLogger().setLevel(logging.DEBUG)

class TodoItem(ndb.Model):
    # Represents the single todo item in datastore
    hash_id = ndb.StringProperty(indexed=True, required=True) # hash of the text field - used as unique id
    text = ndb.StringProperty(indexed=False, required=True) # the text itself
    date = ndb.DateTimeProperty(auto_now_add=True) # date/time when item is added
    username = ndb.UserProperty(indexed=True, required=True) # user whom this item belongs to
    
class DoneItem(ndb.Model):
    # Represents the single done item in datastore
    hash_id = ndb.StringProperty(indexed=False, required=True) # hash of the text field - used as unique id
    text = ndb.StringProperty(indexed=False, required=True) # the text itself
    date = ndb.DateTimeProperty(auto_now_add=True) # date/time when item is added
    username = ndb.UserProperty(indexed=True, required=True)  # user whom this item belongs to
    datetext = ndb.StringProperty(indexed=True, required=True) # human readable date when it's added. got via get_string_date()

def get_hash(text):
    # Return SHA1 of the string text
    itemhash = hashlib.sha1(text.encode("utf8"))
    return itemhash.hexdigest()

def donelist_key(donelist_name=DEFAULT_DONELIST_NAME):
    # Return datastore key for done items list
    return ndb.Key("DONElist", donelist_name)

def get_doneitems(username, all_items=False):
    # Return list of all done items in datastore
    doneitems = []
    query = DoneItem.query(ancestor=donelist_key(DEFAULT_DONELIST_NAME + get_hash(username))).order(-DoneItem.date)
    if all_items == False:
        items = query.fetch(3)
    else:
        items = query.fetch()
    for i in items:
        doneitems.append({
                          "datetext": str(i.datetext),
                          "lines": filter(None, unicode(i.text).splitlines()),
                          })
    return doneitems

def todolist_key(todolist_name=DEFAULT_TODOLIST_NAME):
    # Return datastore key for todo items list
    return ndb.Key("TODOlist", todolist_name)

def get_todoitems(username):
    # Return list of all todo items in datastore
    todoitems = []
    items = TodoItem.query(ancestor=todolist_key(DEFAULT_TODOLIST_NAME + get_hash(username))).order(TodoItem.date).fetch()
    for i in items:
        todoitems.append({
                          "name": get_hash(i.text),
                          "value": "checked",
                          "date": str(i.date),
                          "text": unicode(i.text),
                          })
    return todoitems

def get_string_date():
    # Return a formated string with current date like 'Fri 07 Jun 2013' 
    return strftime("%a %d %b %Y", localtime())

class UsersWrapper(object):
    # Wrapper class around users. Depending on USE_AUTH_USER work with users
    @classmethod
    def get_user(self):
        if USE_AUTH_USER:
            return users.get_current_user()
        else:
            return users.User("v.pupkin@rogaico.ru")
        
    @classmethod
    def get_login_url(self, request_uri):
        if USE_AUTH_USER:
            return users.create_login_url(request_uri)
        else:
            return "/"
    
    @classmethod    
    def get_logout_url(self, request_uri):
        if USE_AUTH_USER:
            return users.create_logout_url(request_uri)
        else:
            return "/"

class MainPage(webapp2.RequestHandler):
    def get(self):
        # Main app page. Check user auth if required and display all interface
        user = UsersWrapper.get_user()
        if user:
            username = user.nickname()
            url = UsersWrapper.get_logout_url(self.request.uri)
        else:
            username = "not logged in"
            url = UsersWrapper.get_login_url(self.request.uri)
            self.redirect(url)
            
        template_values = {
                           "current_user": username,
                           "login_url": url,
                           "login_text": "Logout \"" + username + "\"" if user else "Login",
                           "todoitems": get_todoitems(username) if user else None,
                           "today": get_string_date(),
                           "doneitems": get_doneitems(username) if user else None,
        }

        template = JINJA_ENVIRONMENT.get_template("content.html")
        self.response.write(template.render(template_values))

class SubmitPage(webapp2.RequestHandler):
    def post(self):
        # Whenever user hits the 'Submit' button go and check which item has to be deleted
        todoitems = get_todoitems(UsersWrapper.get_user().nickname())
        for i in todoitems:
            hash_id = i["name"]
            text = self.request.get(hash_id)
            if(text == "checked"):
                querry = TodoItem.query(TodoItem.hash_id == hash_id)
                items = querry.fetch()
                for item in items:
                    item.key.delete()
        self.redirect("/")
        
class AddPage(webapp2.RequestHandler):
    def post(self):
        # Add new todo item here
        item_text = self.request.get("task_text")
        item = TodoItem(parent=todolist_key(DEFAULT_TODOLIST_NAME + get_hash(UsersWrapper.get_user().nickname())))
        item.text = item_text
        item.hash_id = get_hash(item_text)
        item.username = UsersWrapper.get_user()
        item.put()
        self.redirect("/")
        
class AddDonePage(webapp2.RequestHandler):
    def post(self):
        # Add new done item
        item_text = self.request.get("done_text")
        string_date = get_string_date()
        items = DoneItem.query(DoneItem.username == UsersWrapper.get_user(), DoneItem.datetext == string_date).fetch()
        if len(items) != 0:
            for item in items:
                item.text += "\n" + item_text
                item.put()
            self.redirect("/")
            return

        item = DoneItem(parent=donelist_key(DEFAULT_DONELIST_NAME + get_hash(UsersWrapper.get_user().nickname())))
        item.text = item_text
        item.datetext = string_date
        item.hash_id = get_hash(item.text)
        item.username = UsersWrapper.get_user()
        item.put()
        self.redirect("/")
        
class AboutPage(webapp2.RequestHandler):
    def get(self):
        # Just get a page about this app
        template_values = {
                           "app_version": os.environ["CURRENT_VERSION_ID"],
        }
        template = JINJA_ENVIRONMENT.get_template("about.html")
        self.response.write(template.render(template_values))
        
class MobilePage(webapp2.RequestHandler):
    def get(self):
        # Just get a page with links to mobile application
        template_values = {           
        }
        template = JINJA_ENVIRONMENT.get_template("mobile_client.html")
        self.response.write(template.render(template_values))

class AllDonesPage(webapp2.RequestHandler):
    def get(self):
        # Open all done items in new page
        user = UsersWrapper.get_user()
        if user:
            username = user.nickname()
        else:
            self.redirect("/")
            return
        template_values = {
                           "doneitems": get_doneitems(username, all_items=True),
        }

        template = JINJA_ENVIRONMENT.get_template("alldones.html")
        self.response.write(template.render(template_values))

class JsonPage(webapp2.RequestHandler):
    # Page used for REST API with mobile app
    def get(self):
        request_type = self.request.get("type")
        if(request_type == "todoitems"): 
            self.response.headers["Content-Type"] = "application/json"
            self.response.out.write(json.dumps(get_todoitems(UsersWrapper.get_user().nickname())))
            logging.debug("todoitems request from " + self.request.remote_addr)
            
    def post(self):
        json_text = self.request.body
        json_list = json.loads(json_text)
        logging.debug("json received:" + str(json_list))
        
        if json_list["value"] == u"checked":
            # It's done! Remove it
            hash_id = json_list["name"]
            querry = TodoItem.query(TodoItem.hash_id == hash_id)
            items = querry.fetch()
            for item in items:
                item.key.delete()
        else:
            # I't new item
            item = TodoItem(parent=todolist_key(DEFAULT_TODOLIST_NAME + get_hash(UsersWrapper.get_user().nickname())))
            item.text = json_list["text"]
            item.hash_id = get_hash(json_list["text"])
            item.username = UsersWrapper.get_user()
            item.put()
           

application = webapp2.WSGIApplication([
    ("/", MainPage),
    ("/submit", SubmitPage),
    ("/add", AddPage),
    ("/about", AboutPage),
    ("/mobile_client", MobilePage),
    ("/add_done", AddDonePage),
    ("/alldones", AllDonesPage),
    ("/json", JsonPage),
], debug=True)

