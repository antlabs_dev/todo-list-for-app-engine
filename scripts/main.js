function submitOnClick() 
{
	document.getElementById("list_form").submit();
}

function onLoad()
{
	hideSubmitButton();
	setDefaultFocus();
	
}

function hideSubmitButton() 
{
	document.getElementById("submit_button").style.visibility = "hidden";
}

function setDefaultFocus()
{
	document.getElementById("add_form_text").task_text.focus();
}

function catchKeyEvent(e)
{
	if(e.ctrlKey && e.keyCode == 13)
	{
		document.getElementById("done_form").submit();
	}		
}

